library(dplyr)
data_1 = read.table("minimization/corrected_rmsd/corrected.rmsd.min.aif5b.gdp.tab")
data_2 = read.table("minimization.2/corrected_rmsd/corrected.rmsd.min.aif5b.gdp.tab")
data_3 = read.table("minimization.3/corrected_rmsd/corrected.rmsd.min.aif5b.gdp.tab")
data_corrected_rmsd_gdp = left_join(x = data_1, y = data_2, by = "V1")
data_corrected_rmsd_gdp = left_join(x = data_corrected_rmsd_gdp, y = data_3, by = "V1")
data_corrected_rmsd_gdp$V2.z = with(data_corrected_rmsd_gdp, (data_corrected_rmsd_gdp$V2.x + data_corrected_rmsd_gdp$V2.y + data_corrected_rmsd_gdp$V2)/3)
data_corrected_rmsd_gdp = data_corrected_rmsd_gdp[order(data_corrected_rmsd_gdp$V2.z),]
data_corrected_rmsd_gdp = as.data.frame(data_corrected_rmsd_gdp)

dotchart(data_corrected_rmsd_gdp$V2.x,labels = data_corrected_rmsd_gdp$V1, pch = 19, cex = .7, main = "Corrected rmsd aif5b gdp state", xlab = "RMSD (A)", bg= "green",xlim = range(data_corrected_rmsd_gdp$V2.x, data_corrected_rmsd_gdp$V2.y, data_corrected_rmsd_gdp$V2, data_corrected_rmsd_gdp$V2.z))
points(data_corrected_rmsd_gdp$V2.y, 1:nrow(data_corrected_rmsd_gdp), col = "red", pch = 19, cex = .7)
points(data_corrected_rmsd_gdp$V2, 1:nrow(data_corrected_rmsd_gdp), col = "blue", pch = 19, cex = .7)
points(data_corrected_rmsd_gdp$V2.z, 1:nrow(data_corrected_rmsd_gdp), col = "green", pch = 19, cex = .7)


