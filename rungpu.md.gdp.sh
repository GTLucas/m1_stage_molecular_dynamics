#!/bin/bash

. /home/gaillard/prog/amber/amber20-21/amber.sh

export CUDA_VISIBLE_DEVICES=0

gluh=$1

name=(aif5b.$gluh.gdp)

echo $name

prevstep='min_rest10'

for step in heat_rest10 equil_npt_rest10 equil_npt_rest1 equil_npt_rest01 equil_npt equil prod1 prod2; do

rst=rst/${prevstep}_$name.rst

ref=" -ref crd/solv_$name.crd"
if [ "$step" = 'equil_npt' ]||[ "$step" = 'equil' ]||[ "$step" = 'prod1' ]||[ "$step" = 'prod2' ]; then
  ref=''
fi

x=" -x mdcrd/${step}_$name.mdcrd"

exe='pmemd.cuda'
if [ "$step" = 'heat_rest10' ]; then
  exe='sander'
fi

$exe -O -i in/$step.in -o out/${step}_$name.out -inf inf/${step}_$name.inf -p top/solv_$name.top -c $rst -r rst/${step}_$name.rst$ref$x
cpptraj -p top/solv_$name.top -y rst/${step}_$name.rst -x pdb/${step}_$name.pdb > log/rst2pdb_${step}_$name.log

prevstep=$step

done
