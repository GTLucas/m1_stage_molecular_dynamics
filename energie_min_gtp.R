library(dplyr)
data_1 = read.table("minimization/energie/energie.min.aif5b.gtp.tab")
data_2 = read.table("minimization.2/energie/energie.min.aif5b.gtp.tab")
data_3 = read.table("minimization.3/energie/energie.min.aif5b.gtp.tab")
data_energy_gtp = left_join(x = data_1, y = data_2, by = "V1")
data_energy_gtp = left_join(x = data_energy_gtp, y = data_3, by = "V1")
data_energy_gtp$V2.z = with(data_energy_gtp, (data_energy_gtp$V2.x + data_energy_gtp$V2.y + data_energy_gtp$V2)/3)
data_energy_gtp = data_energy_gtp[order(data_energy_gtp$V2.z),]
data = as.data.frame(data_energy_gtp)


dotchart(data_energy_gtp$V2.x,labels = data_energy_gtp$V1, pch = 19, cex = .7, main = "Energy aif5b gtp state", xlab = "Energy total (kcal/mol)", bg= "green",xlim = range(data_energy_gtp$V2.x, data_energy_gtp$V2.y, data_energy_gtp$V2, data_energy_gtp$V2.z))
points(data_energy_gtp$V2.y, 1:nrow(data_energy_gtp), col = "red", pch = 19, cex = .7)
points(data_energy_gtp$V2, 1:nrow(data_energy_gtp), col = "blue", pch = 19, cex = .7)
points(data_energy_gtp$V2.z, 1:nrow(data_energy_gtp), col = "green", pch = 19, cex = .7)